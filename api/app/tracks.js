const express = require('express');
const axios = require('axios');

const getTracks = async (req, res) => {
    try {
        const response = await axios.get(`https://api.deezer.com/search?q=${req.params.name}`);
        const artistId = response.data.data[0].artist.id;
        const topFiveTrack = await axios.get(`https://api.deezer.com/artist/${artistId}/top?limit=5`)
        return res.send(topFiveTrack.data.data.map(item => {
            return {
                title: item.title,
                link: item.link,
                duration: item.duration,
                artist: item.artist.name,
                album: item.album.title,
                albumCover: item.album.cover
            }
        }))
    } catch (e) {
        return res.send({message: 'No records found'})
    }
}

const createRouter = () => {
    const router = express.Router();

    router.get('/:name', getTracks);

    return router;
};

module.exports = createRouter;
