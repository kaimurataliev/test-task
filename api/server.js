const express = require('express');
const cors = require('cors');
const app = express();

const tracks = require('./app/tracks');


const port = 8000;


app.use(cors());
app.use(express.json());

app.use('/tracks', tracks());

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});
