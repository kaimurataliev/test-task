import React, {useEffect, useState} from "react";
import SearchBar from "../../components/SearchBar/SearchBar";
import {useDispatch, useSelector} from "react-redux";
import TrackComponent from "../../components/TrackComponent/TrackComponent";
import {getTracks} from "../../store/actions";

const Tracks = () => {

    const [search, setSearch] = useState('');
    const dispatch = useDispatch();
    const tracks = useSelector(state => state.tracks)
    const isLoading = useSelector(state => state.isLoading);

    useEffect(() => {
        if(search !== '') {
            dispatch(getTracks(search))
        }
    }, [dispatch, search])

    const changeSearch = event => {
        setSearch(event.target.value);
    }

    return (
        <>
            <div className={'header'}>
                <SearchBar search={search} changeSearch={changeSearch}/>
            </div>
            <div className={'container'}>
                {isLoading ? <h2 className={'loading'}>Загрузка...</h2> : tracks && tracks.map((track, index) => {
                    return (
                        <TrackComponent key={index} track={track}/>
                    )
                })}
            </div>
        </>
    )
}

export default Tracks;
