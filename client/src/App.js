import './App.css';
import Tracks from "./containers/Tracks/Tracks";
import {NotificationContainer} from 'react-notifications';

function App() {
    return (
        <>
            <NotificationContainer/>
            <Tracks/>
        </>
    );
}

export default App;
