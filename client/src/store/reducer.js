import {GET_TRACKS_SUCCESS, GET_TRACKS_REQUEST} from "./actions";

const initialState = {
    tracks: null,
    isLoading: false
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_TRACKS_SUCCESS:
            return {...state, tracks: action.data};
        case GET_TRACKS_REQUEST:
            return {...state, isLoading: !state.isLoading}
        default:
            return state;
    }
}

export default reducer;
