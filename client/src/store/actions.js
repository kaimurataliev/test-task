import axios from "axios";
import {NotificationManager} from 'react-notifications';
export const GET_TRACKS_SUCCESS = 'GET_TRACKS_SUCCESS';
export const GET_TRACKS_REQUEST = 'GET_TRACKS_REQUEST';

const getTracksSuccess = data => ({type: GET_TRACKS_SUCCESS, data});
const getTracksRequest = () => ({type: GET_TRACKS_REQUEST});


export const getTracks = name => {
    return async dispatch => {
        dispatch(getTracksRequest())
        try {
            const response = await axios.get(`http://localhost:8000/tracks/${name}`);
            dispatch(getTracksRequest())
            dispatch(getTracksSuccess(response.data));
        } catch (e) {
            dispatch(getTracksRequest())
            NotificationManager.error('No tracks found')
            console.log(e);
        }
    }
}
