import React from 'react';
import './TrackComponent.css';

const TrackComponent = (props) => {
    return (
        <a className={'box'} href={props.track.link}>
            <img src={props.track.albumCover} alt="cover" className={'cover'}/>
            <div className={'info'}>
                <p style={{fontSize: '16px'}}>{props.track.title}</p>
                <p style={{fontSize: '14px'}}><span style={{color: '#cecece'}}>Исполнители:</span> {props.track.artist}</p>
            </div>
            <p>{Math.round(props.track.duration / 60)}</p>
        </a>
    );
};

export default TrackComponent;
