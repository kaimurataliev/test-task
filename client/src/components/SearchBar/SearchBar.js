import React from 'react';

const SearchBar = ({changeSearch, search, searchTracksHandler}) => {
    return (
        <input
            className={'search-bar'}
            type="text"
            value={search}
            onChange={changeSearch}
            placeholder={'Введите имя исполнителя'}
        />
    )
}

export default SearchBar;
